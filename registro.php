<?php
include("conexion_bd.php");
?>
    <title>Registro Parcial#2</title>
    <link rel="stylesheet" type="text/css" href="sistema/css/login.css">
    <link rel="stylesheet" type="text/css" href="sistema/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="sistema/js/login.js"></script>

<div class="overlay">
    <!--ALERT-->
      <?php if (isset($_SESSION['message'])) { ?>
      <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
        <?= $_SESSION['message']?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php session_unset(); } ?>

<form action="sistema/guardar.php" method="POST">
   <div class="con">
   <header class="head-form">
      <button type="button" class="btn submit frgt-pass"><a href="index.php" style="text-decoration: none; color: black; display:flex; float: right">Ir a Login</a></button>
      <h2>Registro</h2>
      <p>Entre aqui usando su usuario y contrase&ntilde;a</p>
   </header>

   <br>
   <div class="field-set">     
      <span class="input-item"><i class="fa fa-user-circle"></i></span>
      <input type="email" name="email" id="email" class="form-input" placeholder="Correo electronico" required>
      <br><br>
     
      <span class="input-item"><i class="fa fa-key"></i></span>
      <input class="form-input" type="password" placeholder="Contrase&ntilde;a" id="clave" name="clave" required>
      <span><i class="fa fa-eye" aria-hidden="true" type="button" id="eye"></i></span>
      <br><br>

      <span class="input-item"><i class="fa fa-user-circle"></i></span>
      <input readonly type="date" name="fecha" id="fecha" class="form-input" value="<?php echo date('Y-m-d'); ?>">
      <br><br>

      <button type="submit" name="usuarios">Registrar Usuario</button>
   </div>

  </div>

</form>
</div>