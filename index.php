<?php
$alert='';
session_start();

if(!empty($_SESSION['active']))
{
    header('location: sistema/');
}
else{
if(!empty($_POST))
{
	if(empty($_POST['email'])||empty($_POST['clave'])){
        $alert='Ingrese su usuario y su clave';
    }else{
        require_once "conexion_bd.php";

        $user=mysqli_real_escape_string($conn,$_POST['email']);
        $pass=md5(mysqli_real_escape_string($conn,$_POST['clave']));

        $query=mysqli_query($conn,"SELECT * FROM avasquez.usuarios_parcial2 WHERE email='$user' AND clave='$pass'");
        mysqli_close($conn);
        $result=mysqli_num_rows($query);

        if($result > 0){
            $data=mysqli_fetch_array($query);
            $_SESSION['active']=true;
            $_SESSION['idUser']=$data['idusuario'];
            $_SESSION['email']=$data['email'];

            header('location: sistema/');
        }
        else{
            $alert="El usuario o la clave son incorrectos";
            session_destroy();
        }
    }
}
} 
?>

    <title>Login Parcial#2</title>
    <link rel="stylesheet" type="text/css" href="sistema/css/login.css">
    <link rel="stylesheet" type="text/css" href="sistema/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="sistema/js/login.js"></script>

<div class="overlay">

<form action"" method="post">

   <div class="con">

   <header class="head-form">
      <h2>Log In</h2>
      <p>Entre aqui usando su usuario y contrase&ntilde;a</p>
   </header>

   <br>
   <div class="field-set">     
         <span class="input-item"><i class="fa fa-user-circle"></i></span>
         <input class="form-input" name="email" id="txt-input" type="text" placeholder="Usuario">
      <br><br>
     
      <span class="input-item"><i class="fa fa-key"></i></span>
      <input class="form-input" type="password" placeholder="Contrase&ntilde;a" id="pwd"  name="clave">
      <span><i class="fa fa-eye" aria-hidden="true"  type="button" id="eye"></i></span>

	<div class="alert"> <?php echo isset($alert) ? $alert : '';?></div>
     
      <br>

      <button class="log-in">Iniciar Sesion</button>
   </div>
  
   <div class="other">
      <button class="btn submits frgt-pass">Olvide mi contrase&ntilde;a</button>
      <button type="button" class="btn submits sign-up"><a href="registro.php" style="text-decoration: none; color: black">Registrarse <i class="fa fa-user-plus" aria-hidden="true"></i></a></button>
   </div>
  </div>
</form>
</div>