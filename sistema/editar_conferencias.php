<?php
session_start();
include("../conexion_bd.php");

//-----------------------------------------------------
if  (isset($_GET['id_conferencia'])) {
  $id_conferencia= $_GET['id_conferencia'];
  $sql = "SELECT * FROM avasquez.conferencias WHERE id_conferencia= '$id_conferencia'";
  $result = mysqli_query($conn, $sql);
  
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombre = $row["nombre"];
    $email_conferencista = $row["email_conferencista"];
  }
}

if (isset($_POST['update'])) {
  $id_conferencia = $_GET['id_conferencia'];
  $nombre = $_POST["nombre"];
  $email_conferencista = $_POST["email_conferencista"];

  $sql = "UPDATE avasquez.conferencias set nombre = '$nombre', email_conferencista = '$email_conferencista' WHERE id_conferencia= '$id_conferencia'";
  mysqli_query($conn, $sql);
  $_SESSION['message'] = 'Modificado Correctamente';
  $_SESSION['message_type'] = 'primary';
  header('Location: registro_conferencias.php');
}
?>

<?php include('includes/header.php'); ?>

<br>
<div class="container p-4">
  <div class="row">
    <div class="col-md-5 mx-auto">
      <div class="card card-body">
      <form action="editar_conferencias.php?id_conferencia=<?php echo $_GET['id_conferencia']; ?>" method="POST" name="f1" id="f1">
       	<legend><strong>Datos de la conferencia</strong></legend>
	<div class="form-group">
          <label><strong>Nombre</strong></label>
          <input type="text" name="nombre" class="form-control" placeholder="Nombre de la conferencia" value="<?php echo $nombre; ?>" required/>
        </div>           
        <div class="form-group">
          <label><strong>Correo del conferencista</strong></label>
	  <select name="email_conferencista" class="form-control" id="exampleSelect1" required/>
              <option readonly><?php echo $email_conferencista; ?></option>
                <?php
                  $query = $conn -> query ("SELECT * FROM avasquez.conferencistas");
                    while ($valores = mysqli_fetch_array($query)) {
                      echo '<option value="'.$valores[email].'">'.$valores[email].'</option>';
                  }
                ?>
            </select>
        </div>
  
        <a href="registro_conferencias.php" class="btn btn-warning">Regresar</a>
        <button class="btn btn-primary" name="update"> Modificar </button>
      </form>
      </div>
    </div>
  </div>
</div>

<?php include('includes/footer.php'); ?>