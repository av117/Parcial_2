<?php
session_start();
include("../conexion_bd.php");

//-----------------------------------------------------
if  (isset($_GET['id_asistente'])) {
  $id_asistente = $_GET['id_asistente'];
  $query = "SELECT * FROM avasquez.asistentes WHERE id_asistente='$id_asistente'";
  $result = mysqli_query($conn, $query);
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombres = $row['nombres'];
    $apellidos = $row['apellidos'];
    $email = $row['email'];
  }
}
?>

<?php include('includes/header.php'); ?>

<br>
<div class="container p-4">
  <div class="row">
    <div class="col-md-6 mx-auto">
      <div class="card card-body">
        <form>
          <legend><strong>Datos del asistente</strong></legend>
          <div class="form-group">
            <label><strong>Nombres</strong></label>
            <input readonly type="text" class="form-control" value="<?php echo $nombres; ?>">
          </div>
          <div class="form-group">
            <label><strong>Apellidos</strong></label>
            <input readonly type="text" class="form-control" value="<?php echo $apellidos; ?>">
          </div>
          <div class="form-group">
            <label><strong>Correo Electronico</strong></label>
            <input readonly type="imagen" class="form-control" value="<?php echo $email; ?>">
          </div>
        </div>

        <div class="modal-footer">
          <a href="registro_asistentes.php" class="btn btn-warning">Regresar</a>
        </div>
	</form>
      </div>
    </div>
  </div>
</div>

<?php include('includes/footer.php'); ?>