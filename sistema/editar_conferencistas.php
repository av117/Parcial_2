<?php
session_start();
include("../conexion_bd.php");

//-------------------------------------------------------
if  (isset($_GET['id_conferencista'])) {
  $id_conferencista= $_GET['id_conferencista'];
  $sql = "SELECT * FROM avasquez.conferencistas WHERE id_conferencista= '$id_conferencista'";
  $result = mysqli_query($conn, $sql);
  
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombres = $row['nombres'];
    $apellidos = $row['apellidos'];
    $profesion = $row['profesion'];
    $biografia = $row['biografia'];
    $email = $row['email'];
  }
}

if (isset($_POST['update'])) {
  $id_conferencista= $_GET['id_conferencista'];
  $nombres = $_POST['nombres'];
  $apellidos = $_POST['apellidos'];
  $profesion = $_POST['profesion'];
  $biografia = $_POST['biografia'];
  $email = $_POST['email'];

  $sql = "UPDATE avasquez.conferencistas set nombres = '$nombres', apellidos = '$apellidos', profesion = '$profesion' , biografia = '$biografia', email = '$email' WHERE id_conferencista= '$id_conferencista'";
  mysqli_query($conn, $sql);
  
  $_SESSION['message'] = 'Modificado Correctamente';
  $_SESSION['message_type'] = 'primary';
  header('Location: registro_conferencistas.php');
}

?>


<?php include('includes/header.php'); ?>

<br>
<div class="container p-4">
  <div class="row">
    <div class="col-md-5 mx-auto">
      <div class="card card-body">
        <form action="editar_conferencistas.php?id_conferencista=<?php echo $_GET['id_conferencista']; ?>" method="POST">
          <legend><strong>Datos del paciente</strong></legend>
          <div class="form-group">
            <label><strong>Nombres</strong></label>
            <input type="text" name="nombres" class="form-control" placeholder="Nombres del conferencista" value="<?php echo $nombres; ?>" required/>
          </div>
          <div class="form-group">
            <label><strong>Apellidos</strong></label>
            <input type="text" name="apellidos" class="form-control" placeholder="Apellidos del conferencista" value="<?php echo $apellidos; ?>" required/>
          </div>
          <div class="form-group">
            <label><strong>Profesion</strong></label>
	    <select name="profesion" class="form-control" required/>
		<option readonly><?php echo $profesion; ?></option>
		<option>Ingeniero(a)</option>
		<option>Maestro(a)</option>
		<option>Contador(a)</option>
		<option>Doctor(a)</option>
	    </select>
          </div>
          <div class="form-group">
            <label><strong>Biografia</strong></label>
	    <textarea name="biografia" rows="3" class="form-control" placeholder="Biografia del conferencista" required/><?php echo $biografia; ?></textarea>
          </div>
          <div class="form-group">
            <label><strong>Email</strong></label>
            <input type="email" name="email" class="form-control" placeholder="Correo del conferencista" value="<?php echo $email; ?>" required/>
          </div>

        <a href="registro_conferencistas.php" class="btn btn-warning">Regresar</a>
        <button class="btn btn-primary" name="update"> Modificar </button>
      </form>
      </div>
    </div>
  </div>
</div>

<?php include('includes/footer.php'); ?>