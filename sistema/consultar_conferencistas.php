<?php
session_start();
include("../conexion_bd.php");

//-------------------------------------------------------
if  (isset($_GET['id_conferencista'])) {
  $id_conferencista= $_GET['id_conferencista'];
  $sql = "SELECT * FROM avasquez.conferencistas WHERE id_conferencista= '$id_conferencista'";
  $result = mysqli_query($conn, $sql);
  
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombres = $row['nombres'];
    $apellidos = $row['apellidos'];
    $profesion= $row['profesion'];
    $biografia = $row['biografia'];
    $email = $row['email'];
  }
}
?>


<?php include('includes/header.php'); ?>

<br>
<div class="container p-4">
  <div class="row">
    <div class="col-md-6 mx-auto">
      <div class="card card-body">
        <form>
          <legend><strong>Datos del conferencista</strong></legend>
          <div class="form-group">
            <label><strong>Nombres</strong></label>
            <input readonly type="text" class="form-control" value="<?php echo $nombres; ?>">
          </div>
          <div class="form-group">
            <label><strong>Apellidos</strong></label>
            <input readonly type="text" class="form-control" value="<?php echo $apellidos; ?>">
          </div>
          <div class="form-group">
            <label><strong>Profesion</strong></label>
            <input readonly type="text" class="form-control" value="<?php echo $profesion; ?>">
          </div>
          <div class="form-group">
            <label><strong>Biografia</strong></label>
	    <textarea readonly name="biografia" rows="3" class="form-control" ><?php echo $biografia; ?></textarea>
          </div>
          <div class="form-group">
            <label><strong>Correo electronico</strong></label>
            <input readonly type="email" class="form-control" value="<?php echo $email; ?>">
          </div>
        </div>

        <div class="modal-footer">
          <a href="registro_conferencistas.php" class="btn btn-warning">Regresar</a>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>

<?php include('includes/footer.php'); ?>