<?php
  if(empty($_SESSION['active']))
 {
      header('location: ../');
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Practica Parcial #2</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <!-- BOOTSTRAP 4 -->
    <link rel="stylesheet" href="https://bootswatch.com/4/flatly/bootstrap.min.css">
    <!-- FONT AWESOEM -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  </head>
  <body>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container-fluid">
    <a class="navbar-brand" href="index.php" style="font-family: Sans serif; font-size: 35px; text-transform: none">PARCIAL #2</a>

    <a href="salir.php" class="navbar-brand"><img class="close" src="imagenes/salir.png" alt="Salir del sistema" title="Salir">
	<?php echo date('Y-m-d');?><span> | </span>
	<?php echo $_SESSION['email'];?>
    </a>
  </div>
</nav>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
      <ul class="navbar-nav me-auto">
        <li class="nav-item active">
          <a class="nav-link" href="index.php">&nbsp;&nbsp;&nbsp<i class="fa fa-home" class="nav-item" aria-hidden="true"></i> Inicio</a>
        </li>
	
        <li class="nav-item">
          <a class="nav-link" href="registro_usuarios.php">&nbsp;&nbsp;&nbsp<i class="fa fa-users" aria-hidden="true"></i> Usuarios</a>
        </li>
	
        <li class="nav-item">
          <a class="nav-link" href="registro_conferencistas.php">&nbsp;&nbsp;&nbsp<i class="fa fa-users" aria-hidden="true"></i> Conferencistas</a>
        </li>
	
        <li class="nav-item">
          <a class="nav-link" href="registro_conferencias.php">&nbsp;&nbsp;&nbsp<i class="fa fa-list" aria-hidden="true"></i> Conferencias</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="registro_asistentes.php">&nbsp;&nbsp;&nbsp<i class="fa fa-users" aria-hidden="true"></i> Asistentes</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
</div>