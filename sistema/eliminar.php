<?php
include("../conexion_bd.php");

if(isset($_GET['id_asistente'])) {
  $id_asistente= $_GET['id_asistente'];
  $sql = "DELETE FROM avasquez.asistentes WHERE id_asistente= '$id_asistente'";
  $result = mysqli_query($conn, $sql);
  if(!$result) {
    die("Query Failed.");
  }

  $_SESSION['message'] = 'Eliminado Correctamente';
  $_SESSION['message_type'] = 'warning';
  header('Location: registro_asistentes.php');
}


if(isset($_GET['id_conferencista'])) {
  $id_conferencista= $_GET['id_conferencista'];
  $sql = "DELETE FROM avasquez.conferencistas WHERE id_conferencista= '$id_conferencista'";
  $result = mysqli_query($conn, $sql);
  if(!$result) {
    die("Query Failed.");
  }

  $_SESSION['message'] = 'Eliminado Correctamente';
  $_SESSION['message_type'] = 'warning';
  header('Location: registro_conferencistas.php');
}


if(isset($_GET['id_conferencia'])) {
  $id_conferencia= $_GET['id_conferencia'];
  $sql = "DELETE FROM avasquez.conferencias WHERE id_conferencia= '$id_conferencia'";
  $result = mysqli_query($conn, $sql);
  if(!$result) {
    die("Query Failed.");
  }

  $_SESSION['message'] = 'Eliminado Correctamente';
  $_SESSION['message_type'] = 'warning';
  header('Location: registro_conferencias.php');
}


if(isset($_GET['idusuario'])) {
  $idusuario = $_GET['idusuario'];
  $sql = "DELETE FROM avasquez.usuarios_parcial2 WHERE idusuario = '$idusuario'";
  $result = mysqli_query($conn, $sql);
  if(!$result) {
    die("Query Failed.");
  }

  $_SESSION['message'] = 'Eliminado Correctamente';
  $_SESSION['message_type'] = 'warning';
  header('Location: registro_usuarios.php');
}

?>