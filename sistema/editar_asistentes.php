<?php
session_start();
include("../conexion_bd.php");

//-------------------------------------------------------
if  (isset($_GET['id_asistente'])) {
  $id_asistente= $_GET['id_asistente'];
  $sql = "SELECT * FROM avasquez.asistentes WHERE id_asistente= $id_asistente;";
  $result = mysqli_query($conn, $sql);

  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombres = $row['nombres'];
    $apellidos = $row['apellidos'];
    $email = $row['email'];
  }
}


if (isset($_POST['update'])) {
  $id_asistente = $_GET['id_asistente'];
  $nombres = $_POST['nombres'];
  $apellidos = $_POST['apellidos'];
  $email = $_POST['email'];
  
  $sql = "UPDATE avasquez.asistentes set nombres = '$nombres', apellidos = '$apellidos', email = '$email' WHERE id_asistente= '$id_asistente'";
  mysqli_query($conn, $sql);

  $_SESSION['message'] = 'Modificado Correctamente';
  $_SESSION['message_type'] = 'primary';
  header('Location: registro_asistentes.php');
}
?>


<?php include('includes/header.php'); ?>

<br>
<div class="container p-4">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="card card-body">
      <form action="editar_asistentes.php?id_asistente=<?php echo $_GET['id_asistente']; ?>" method="POST">
        <legend><strong>Datos del asistente</strong></legend>
        <div class="form-group">
          <label><strong>Nombres</strong></label>
          <input type="text" name="nombres" class="form-control" placeholder="Nombres" value="<?php echo $nombres; ?>" required/>
        </div>
        <div class="form-group">
          <label><strong>Apellidos</strong></label>
          <input type="text" name="apellidos" class="form-control" placeholder="Apellidos" value="<?php echo $apellidos; ?>" required/>
        </div>
        <div class="form-group">
          <label><strong>Correo electronico</strong></label>          
          <input type="email" name="email" class="form-control" placeholder="Correo electronico" value="<?php echo $email; ?>" required/>
        </div>
        
        <a href="registro_asistentes.php" class="btn btn-warning">Regresar</a>
        <button class="btn btn-primary" name="update"> Modificar </button>
      </form>
      </div>
    </div>
  </div>
</div>

<?php include('includes/footer.php'); ?>