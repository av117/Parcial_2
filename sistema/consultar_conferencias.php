<?php
session_start();
include("../conexion_bd.php");

//-----------------------------------------------------
if  (isset($_GET['id_conferencia'])) {
  $id_conferencia= $_GET['id_conferencia'];
  $sql = "SELECT * FROM avasquez.conferencias WHERE id_conferencia= '$id_conferencia'";
  $result = mysqli_query($conn, $sql);
  
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombre = $row["nombre"];
    $email_conferencista = $row["email_conferencista"];
  }
}
?>

<?php include('includes/header.php'); ?>

<br>
<div class="container p-4">
  <div class="row">
    <div class="col-md-6 mx-auto">
      <div class="card card-body">
        <form>
          <legend><strong>Datos de la conferencia</strong></legend>
          <div class="form-group">
            <label><strong>Nombre</strong></label>
            <input readonly type="text" class="form-control" value="<?php echo $nombre; ?>">
          </div>
          <div class="form-group">
            <label><strong>Correo del conferencista</strong></label>
            <input readonly type="email" class="form-control" value="<?php echo $email_conferencista; ?>">
          </div>
        </div>

        <div class="modal-footer">
          <a href="registro_conferencias.php" class="btn btn-warning">Regresar</a>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>

<?php include('includes/footer.php'); ?>